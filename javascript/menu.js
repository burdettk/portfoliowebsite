$( document ).ready(function() {
  $('.hamburger').on('click', function(ev){               // when clicking the hamburger button, make the nav 100% width
    ev.preventDefault();
    $('#nav').css('width', '100%');           // full size width nav when viewed clicked hamburger button on mobile
    $('#close').removeClass('hidden');       // removing the slass declared as hidden for the 'x' close button for the nav
  });
  $('#close').on('click', function(ev) {          // when clicking the close button, make the width of the nav from 100% to 0
    ev.preventDefault();
    $('#nav').css('width', '0');
    $('#close').addClass('hidden');                   // hide the nav
  });
});
